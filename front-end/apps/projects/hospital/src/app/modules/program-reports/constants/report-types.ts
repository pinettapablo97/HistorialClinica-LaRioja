export const PROGRAM_REPORT_TYPES = [
    {
        description: 'Reporte de epidemiología - Vigilancia Epidemiológica',
        id: 1,
    },
    {
        description: 'Reporte de epidemiología - Notificación Colectiva',
        id: 2,
    }
]
